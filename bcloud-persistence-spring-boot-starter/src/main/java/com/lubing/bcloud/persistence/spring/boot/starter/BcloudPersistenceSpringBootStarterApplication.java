package com.lubing.bcloud.persistence.spring.boot.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BcloudPersistenceSpringBootStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(BcloudPersistenceSpringBootStarterApplication.class, args);
    }

}
