package com.lubing.cloud.aservicesms.controller;

import com.lubing.web.spring.boot.starter.exception.AjaxResponse;
import com.lubing.web.spring.boot.starter.exception.CustomException;
import com.lubing.web.spring.boot.starter.exception.CustomExceptionType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lubing
 * @date 2021/4/14 22:47
 */
@RestController
@RequestMapping("/sms")
public class SmsController {
    @Value("${server.port}")
    private String serverPort;

    /**
     * 模拟短信发送
     * @param phoneNo
     * @param content
     * @return 短信发送结果
     */
    @PostMapping(value = "/send")
    public AjaxResponse send(@RequestParam String phoneNo,
                             @RequestParam String content) {
        if(content.isEmpty() || phoneNo.isEmpty()){
            throw new CustomException(CustomExceptionType.USER_INPUT_ERROR,
                    "消息内容或手机号不能为空！");
        }
        System.out.println(serverPort + ":" + content);

        return AjaxResponse.success("短消息发送成功！");
    }
}
