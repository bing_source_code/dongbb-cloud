package com.lubing.cloud.aservice.rbac.service;

import com.lubing.web.spring.boot.starter.exception.CustomException;
import com.lubing.web.spring.boot.starter.exception.CustomExceptionType;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author lubing
 * @date 2021/4/15 0:19
 */
@Service
public class SysuserService {
    @Resource
    private SmsService smsService;
    @Value("${user.init.password}")
    private String password;

    /**
     * 根据用户的userId重置该用户的密码
     * @param userId
     */
    public void pwdreset(Integer userId){
        if(userId == null){
            throw new CustomException(CustomExceptionType.USER_INPUT_ERROR,
                    "重置密码必须带主键");
        }else{
           /* SysUser sysUser = sysUserMapper.selectByPrimaryKey(userId);

            //去数据库sys_config表加载用户初始化默认密码，key为user.init.password
            String defaultPwd = dbLoadSysConfig.getConfigItem("user.init.password");
            sysUser.setPassword(passwordEncoder.encode(defaultPwd));
            //更新该用户密码为默认初始化密码
            sysUserMapper.updateByPrimaryKeySelective(sysUser);*/
            //看上去就像是本地调用smsService接口方法，实际是发送http请求调用远程服务
            smsService.send("18076903045","您好，管理员已经将您的密码重置为:" + password);
        }
    }
}
