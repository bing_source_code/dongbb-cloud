package com.lubing.cloud.aservice.rbac;

import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication(scanBasePackages = "com.lubing.cloud")
//@EnableEurekaClient//如果服务注册中心是eureka，就需要在服务启动类加上@EnableEurekaClient注解，实现服务发现。
@EnableDiscoveryClient//如果是其他的注册中心，那么更推荐使用@EnableDiscoveryClient，该注解更加的通用。
@EnableFeignClients
@EnableApolloConfig
public class AserviceRbacApplication {

    public static void main(String[] args) {
        SpringApplication.run(AserviceRbacApplication.class, args);
    }

}
