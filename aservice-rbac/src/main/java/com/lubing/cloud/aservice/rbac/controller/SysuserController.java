package com.lubing.cloud.aservice.rbac.controller;

import com.lubing.cloud.aservice.rbac.service.SysuserService;
import com.lubing.web.spring.boot.starter.exception.AjaxResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author lubing
 * @date 2021/4/15 0:18
 */
@RestController
@RequestMapping("/sysuser")
public class SysuserController {
    @Resource
    private SysuserService sysuserService;

    @PostMapping(value = "/pwd/reset")
    public AjaxResponse pwdreset(@RequestParam Integer userId) {
        sysuserService.pwdreset(userId);
        return AjaxResponse.success("重置密码成功!");
    }
}
