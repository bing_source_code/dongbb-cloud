package com.lubing.cloud.aservice.rbac.service;

import com.lubing.web.spring.boot.starter.exception.AjaxResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author lubing
 * @date 2021/4/15 0:17
 */
@FeignClient("ASERVICE-SMS")
public interface SmsService {

    @PostMapping(value = "/sms/send")
    AjaxResponse send(@RequestParam("phoneNo") String phoneNo,
                      @RequestParam("content") String content);

}