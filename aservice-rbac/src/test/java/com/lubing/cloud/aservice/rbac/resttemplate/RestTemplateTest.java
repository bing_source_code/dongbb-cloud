package com.lubing.cloud.aservice.rbac.resttemplate;

import com.lubing.web.spring.boot.starter.exception.AjaxResponse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author lubing
 * @date 2021/4/17 17:39
 */
@ExtendWith(SpringExtension.class)  //Junit5
@SpringBootTest
public class RestTemplateTest {

    @Resource
    private RestTemplate restTemplate;

    @Test
    void httpPostForObject() throws Exception {
        //发送远程http请求的url
        String url = "http://ASERVICE-SMS/sms/send";
        //发送到远程服务的参数
        MultiValueMap<String, Object> params = new LinkedMultiValueMap<>();
        params.add("phoneNo", "13214409773");
        params.add("content", "RestTemplate测试远程服务调用");

        //通过RestTemplate对象发送postForObject请求
        AjaxResponse ajaxResponse = restTemplate.postForObject(url, params, AjaxResponse.class);

        System.out.println(ajaxResponse);

        /*//通过RestTemplate对象发送postForEntity请求
        ResponseEntity<AjaxResponse> entities = restTemplate.postForEntity(url, params, AjaxResponse.class);

        System.out.println(entities.getBody());

        //查看响应的状态码
        System.out.println(entities.getStatusCodeValue());

        //查看响应头
        HttpHeaders headMap = entities.getHeaders();
        for(Map.Entry<String, List<String>> m : headMap.entrySet()){
            System.out.println(m.getKey() + ": " + m.getValue());
        }*/
    }
}